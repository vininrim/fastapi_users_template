# Fastapi Users Template



## Getting started

1) Создать базу данных через pgAdmin
2) Внести данные о БД в файл `.env` (пример см. ниже)
3) Установить зависимости из файла `requirements.txt` командой `pip install -r requirements.txt`
4) Выполнить миграции
   1) Если папка `migrations/versions` пуста, то необходимо выполнить миграции командой `alembic revision --autogenerate` и перейти к шагу ниже
   2) Если папка `migrations/versions` не пустая, то необходимо применить миграции командой `alembic upgrade head`
5) Запустить сервер командой `uvicorn main:app --reload`

## Конфигурация `.env`

```dotenv
DB_HOST=localhost
DB_PORT=5432
DB_USER=postgres
DB_PASSWORD=postgres
DB_DATABASE_NAME=fastapi_auth_database
```

Все парамтеры интуитивно понятные

`DB_DATABASE_NAME` - Название базы данных, которую ты создал через pgAdmin ранее

## Добавление новых моделей в миграции

Если вы создали новые модели и хотите добавить их к миграции, то необходимо, чтобы выши модели содержали `metadata`

- При использовании ООП
  - Вы должны создать класс модели и наследоваться от базового, все `metadata` будут находиться в классе `Base` и применятся ко всем классам, которые будут наследованы от него 
  ```python
  
    from sqlalchemy.orm import DeclarativeBase
  
    class Base(DeclarativeBase):
        pass
    
    class NewModel(Base):
        id: int = Column(Integer, primary_key=True, autoincrement=True, index=True)
        name: str = Column(String(length=50), nullable=False)
   ```
- При использовании другого подхода
  - В таком случае ищите информацию в документации

Для отслеживания изменений в моделях, нужно передать метаданные о них alembic

Для этого нужно открыть файл `migrations/env.py`, где найти `target_metadata`, туда и записывать метаданные

```python
from src.auth.database import Base as NewModels

target_metadata = [AuthModel.metadata]
```

При добавлении новых моделей из других модулей, нужно будет просто добавить в список `target_metadata` еще один элемент с метаданными