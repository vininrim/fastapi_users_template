import uuid

from fastapi_users import schemas


class UserRead(schemas.BaseUser[uuid.UUID]):
    id: int
    first_name: str
    second_name: str
    last_name: str


class UserCreate(schemas.BaseUserCreate):
    first_name: str
    second_name: str
    last_name: str
